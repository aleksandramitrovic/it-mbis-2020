# Tehnologije

1. Python 3.9
2. PySide2
3. PyMySQL

## Koraci

1. Instalirati Python
2. Kreirati virtuelno okruženje
3. Aktivirati virtuelno okruženje
4. Instalirati PySide2
5. Instalirati PyMySQL
6. Pokrenuti program

