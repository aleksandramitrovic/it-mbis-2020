import sys
from PySide2 import QtWidgets, QtGui
from client.main_window import MainWindow
from server.db_handler import DBHandler


if __name__ == "__main__":
    # handler = DBHandler()
    # handler.insert_one("naseljeno_mesto", handler.get_column_names("naseljeno_mesto"), ["SRB", 24, "Loznica", 15300])
    # handler.update_one("naseljeno_mesto", ["NM_NAZIV", "NM_PTT_OZNAKA"], ["Neko mesto", 16000], "NM_OZNAKA", 23)
    # data = handler.get_columns("naseljeno_mesto")#handler.get_all_condition("naseljeno_mesto", "DR_OZNAKA", '\'SRB\'')
    # data = handler.get_foreign_keys("naseljeno_mesto")
    # print(data)
    app = QtWidgets.QApplication(sys.argv)
    main_window = MainWindow(title="MBIS", size=(1024, 720), icon=QtGui.QIcon("client/icons/icons8-edit-file-64.png"))
    main_window.show()
    sys.exit(app.exec_())