# import mysql.connector
import pymysql.cursors

class DBHandler:
    def __init__(self, metadata={}) -> None:
        if len(metadata) == 0:
            self.metadata = {
                "login": {
                    "user": "root",
                    "password": "root",
                    "host": "127.0.0.1",
                    "database": "bis"
                }
            }
        else:
            self.metadata = metadata
        self.connect()

    def connect(self):
        # Connect to the database
        self.connection = pymysql.connect(host=self.metadata["login"]["host"],
                                    user=self.metadata["login"]["user"],
                                    password=self.metadata["login"]["password"],
                                    database=self.metadata["login"]["database"],
                                    cursorclass=pymysql.cursors.DictCursor)
        self.cursor = self.connection.cursor()
    
    def get_all(self, table_name):
        query = "SELECT * FROM " + table_name + ";"
        self.cursor.execute(query)
        return self.cursor.fetchall()

    def get_all_condition(self, table_name, condition, value):
        # uslov = value
        # (uslov1, uslov2) = (vrednost1, vrednost2)
        query = "SELECT * FROM " + table_name + " WHERE " + condition + "=" + value + ";"
        self.cursor.execute(query)
        return self.cursor.fetchall()

    def get_tables(self):
        query = "SHOW TABLES FROM " + self.metadata["login"]["database"] + ";"
        self.cursor.execute(query)
        return self.cursor.fetchall()

    def get_table_names(self):
        tables = self.get_tables()
        table_names = []
        for t in tables:
            table_names.append(t["Tables_in_"+self.metadata["login"]["database"]])
        return table_names

    def get_columns(self, table_name):
        query = "SHOW COLUMNS FROM " + table_name + " FROM " + self.metadata["login"]["database"] + ";"
        self.cursor.execute(query)
        return self.cursor.fetchall()

    def get_column_names(self, table_name):
        columns = self.get_columns(table_name)
        filtered = []
        for c in columns:
            filtered.append(c["Field"])
        return filtered

    def get_primary_keys(self, table_name):
        columns = self.get_columns(table_name)
        keys = []
        for c in columns:
            if c["Key"] == "PRI":
                keys.append(c["Field"])
        return keys

    def get_foreign_keys(self, table_name):
        query = "SHOW CREATE TABLE " + table_name + ";" # izraz za kreiranje tabele
        self.cursor.execute(query)
        result = self.cursor.fetchone()
        elements = result["Create Table"].split("\n")
        fk_constraints = []
        for e in elements:
            e = e.strip()
            if "FOREIGN KEY" in e:
                print(e)
                result = self.parse_foreign_key(e)
                result["pt_name"] = table_name
                if len(result) > 1:
                    fk_constraints.append(result)
        return fk_constraints

    def parse_foreign_key(self, statement):
        result = {
            "fk_name": None,
            "pt_name": None,
            "pt_column": None, # moze biti lista
            "ct_column": None, # moze biti lista
            "ct_name": None
        }
        # TODO: Napraviti parsiranje izraza radi dobavljanja kljuca

        return result


    def insert_one(self, table_name, columns, values):
        """
        Formira i izvrsava upit za dodavanje jednog reda u tabelu.
        :param table_name: naziv tabele u koju se red dodaje
        :param columns: lista naziva kolona
        :param values: lista vrednosti
        """
        # INSERT INTO table (column1, column2, ...) VALUES (value1, value2, ...)
        query = "INSERT INTO " + table_name + " ("
        for i in range(len(columns)-1):
            query += columns[i] + ","
        query +=  columns[len(columns)-1] + ") VALUES ("
        for i in range(len(values)-1):
            query += "%s, "
        query += "%s);"
        print(query)
        self.cursor.execute(query, values)
        self.connection.commit()

    def update_one(self, table_name, columns, values, condition, condition_value):
        """
        Formira i izvrsava upit za izmenu jednog reda u tabeli koji zadovoljava uslov.
        :param table_name: naziv tabele u kojoj se menja red
        :param columns: lista naziva kolona
        :param values: lista vrednosti koje se menjaju
        :param condition: kolona po kojoj se proverava trazeni red
        :param condition_value: vrednost kolone reda kojem se menjaju podaci
        """
        # FIXME: obezbediti da se mogu prosledjivati NULL vrednosti umesto string None
        # UPDATE table SET column1=value1, column2=value2 WHERE condition=condition_value
        query = "UPDATE " + table_name + " SET "
        for i in range(len(columns)-1):
            query += columns[i] + " = %s, "
        query += columns[len(columns)-1] + " = %s "
        query += "WHERE " + condition + " = %s;"
        
        args = values
        args.extend([condition_value])
        print(query, args)
        self.cursor.execute(query, args)
        self.connection.commit()

    def delete_one(self, table_name, condition, condition_value):
        """
        Formira i izvrsava upit za brisanje jednog reda u tabeli koji zadovoljava uslov.
        :param table_name: naziv tabele iz koje se brise red
        :param condition: naziv kolone (primarnog kljuca) po kojoj se vrsi izbor reda za brisanje
        :param condition_value: vrednost kolone (primarnog kljuca) za red koji se brise
        """
        # FIXME: omoguciti da se moze po vise kolona izvrsiti selekcija reda za brisanje
        # Ova metoda se moze iskoristiti i za brisanje vise redova odjednom, ako za
        # condition i value su prosledjene kolona i vrednost koje nisu deo primarnog kljuca
        query = "DELETE FROM " + table_name + " WHERE "+ condition + "=%s;"
        print(query, [condition_value])
        self.cursor.execute(query, [condition_value])
        self.connection.commit()

