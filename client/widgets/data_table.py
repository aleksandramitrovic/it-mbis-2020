from PySide2 import QtWidgets, QtGui


class DataTable(QtWidgets.QWidget):
    def __init__(self, parent=None, parent_table=None, parent_link_column=None, link_column=None, table_name="", 
        add_dialog=None, edit_dialog=None, delete_dialog=None):
        super().__init__(parent)
        # povezana (roditeljska) tabela
        self._parent_table = parent_table
        self._parent_link_column = parent_link_column
        self._link_column = link_column
        # dijalog za dodavanje
        self._add_dialog = add_dialog
        # dijalog za editovanje
        self._edit_dialog = edit_dialog
        # dijalog za brisanje
        self._delete_dialog = delete_dialog
        # sistem redjanja elemenata u nasem widget-u je vertikalni
        self._layout = QtWidgets.QVBoxLayout()
        # toolbar
        self._toolbar = QtWidgets.QToolBar()
        # labela naziva tabele
        self._name_label = QtWidgets.QLabel(table_name if table_name != "" else "Table")
        # tabela
        self._table = QtWidgets.QTableWidget()
        self._table.setSortingEnabled(True)
        # akcije nad tabelom
        self._actions = {
            "add_row": QtWidgets.QAction(QtGui.QIcon("client/icons/table-insert-row.png"), "Add row"),
            "edit_row": QtWidgets.QAction(QtGui.QIcon("client/icons/table--pencil.png"), "Edit row"),
            "remove_row": QtWidgets.QAction(QtGui.QIcon("client/icons/table-delete-row.png"), "Remove row"),
            "previous": QtWidgets.QAction(QtGui.QIcon("client/icons/arrow-step-out.png"), "Previous"),
            "next": QtWidgets.QAction(QtGui.QIcon("client/icons/arrow-step.png"), "Next"),
            "first": QtWidgets.QAction(QtGui.QIcon("client/icons/arrow-stop-090.png"), "First"),
            "last": QtWidgets.QAction(QtGui.QIcon("client/icons/arrow-stop-270.png"), "Last"),
            "find": QtWidgets.QAction(QtGui.QIcon("client/icons/magnifier.png"), "Find"),
        }
        
        # dodavanje akcija u toolbar
        self._toolbar.addActions(list(self._actions.values()))

        # uvezivanje akcija sa funkcijama koje se pozivaju prilikom trigerovanja akcije
        self._bind_actions()


        # podesavanja tabele
        # selekcija samo jednog elementa
        self._table.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        # element koji se selektuje je na nivou reda
        self._table.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)

        # dodavanje toolbar-a u zaglavlje
        self._layout.setMenuBar(self._toolbar)
        # redjanje elemenata u layout
        self._layout.addWidget(self._name_label)
        self._layout.addWidget(self._table)

        self.setLayout(self._layout)

    def _bind_actions(self):
        self._actions["add_row"].triggered.connect(self._add_row)
        self._actions["edit_row"].triggered.connect(self._edit_row)
        self._actions["remove_row"].triggered.connect(self._delete_row)
        self._actions["previous"].triggered.connect(self._previous_row)
        self._actions["next"].triggered.connect(self._next_row)
        self._actions["first"].triggered.connect(self._first_row)
        self._actions["last"].triggered.connect(self._last_row)

        if self._parent_table is not None:
            self._parent_table.itemSelectionChanged.connect(self.filter_data)

        if self._add_dialog is not None:
            self._add_dialog.accepted.connect(self._add_data)

        # dodajemo uvezivanje akcije za prikaz dijaloga na klik akcije
        if self._edit_dialog is not None:
            self._edit_dialog.accepted.connect(self._edit_data)

        if self._delete_dialog is not None:
            self._delete_dialog.accepted.connect(self._delete_data)


    def _add_data(self):
        """
        Kada se prihvati dijalog za dodavanje elementa vrsi se dodavanje u bazu podataka i
        azuriranje tabele.
        """
        # FIXME: kasnije prepraviti da postuje MVC princip
        self.parent().insert_data(self.metadata["name"], self._add_dialog.data)
        self._table.insertRow(self._table.rowCount())
        row = self._table.rowCount() - 1
        column = 0
        for v in self._add_dialog.data.values():
            item = QtWidgets.QTableWidgetItem(str(v))
            self._table.setItem(row, column, item)
            column += 1

    def _edit_data(self):
        """
        Kada se prihvati dijalog za editovanje potrebno je prepisati stare podatke u tabeli
        novim iscitanim iz forme dijaloga.
        """
        # FIXME: izmeniti da se trazi po primarnom kljucu (ne samo oznaka drzave)
        oznaka = self._table.item(self._table.currentRow(), 0) # oznaka selektovane drzave u tabeli

        self.parent().edit_data(self.metadata["name"], self._edit_dialog.data, self.metadata["pk"], [oznaka.text()])        
        row = self._table.currentRow()
        column = 0
        for v in self._edit_dialog.data.values():
            item = QtWidgets.QTableWidgetItem(str(v))
            self._table.setItem(row, column, item)
            column += 1

    def _delete_data(self):
        """
        """
        # FIXME: ne mora da znaci da ce primarni kljuc uvek biti u 0-toj koloni i da je samo jedan
        pk_value = self._table.item(self._table.currentRow(), 0).text()
        self.parent().delete_data(self.metadata["name"], self.metadata["pk"], [pk_value])
        self._table.removeRow(self._table.currentRow())

    def _add_row(self):
        if self._add_dialog is not None:
            self._add_dialog.exec_()


    def _edit_row(self):
        if self._edit_dialog is not None:
            # TODO: podesiti data u dijalogu da ima sadrzaj od trenutnog reda selektovanog u tabeli
            row = self._table.currentRow()
            column = 0
            data = {}
            for md in self.metadata["columns"]:
                data[md] = self._table.item(row, column).text()
                column += 1
            self._edit_dialog.populate_form(data)
            self._edit_dialog.exec_()

    def _delete_row(self):
        if self._delete_dialog is not None:
            self._delete_dialog.exec_()

    def _next_row(self):
        item = self._table.currentItem()
        if item is None:
            return
        max_row = self._table.rowCount()
        if item.row() + 1 < max_row:
            self._table.setCurrentItem(self._table.item(item.row()+1, 0))
    
    def _previous_row(self):
        item = self._table.currentItem()
        if item is None:
            return
        if item.row() - 1 >= 0:
            self._table.setCurrentItem(self._table.item(item.row()-1, 0))

    def _first_row(self):
        self._table.setCurrentItem(self._table.item(0, 0))

    def _last_row(self):
        self._table.setCurrentItem(self._table.item(self._table.rowCount()-1, 0))

    def set_table_header(self, header=[]):
        self._table.setColumnCount(len(header))
        self._table.setHorizontalHeaderLabels(header)

    def populate_table(self, data=None, metadata=None):
        self.data = data
        self.metadata = metadata

        self._table.clearContents()
        self._table.setColumnCount(len(metadata["columns"]))
        self._table.setRowCount(len(data))
        self._table.setHorizontalHeaderLabels(metadata["columns"])
        row = column = 0
        for e in self.data:
            for v in e.values():
                item = QtWidgets.QTableWidgetItem(str(v))
                self._table.setItem(row, column, item)
                column += 1
            row += 1
            column = 0

    def set_dialogs(self, add_dialog=None, edit_dialog=None, delete_dialog=None):
        self._add_dialog = add_dialog
        self._edit_dialog = edit_dialog
        # self._delete_dialog = delete_dialog

    def filter_data(self):
        self._table.clearContents()
        self._table.setRowCount(0)
        item = self._parent_table.currentItem()
        filter_item = self._parent_table.item(item.row(), self._parent_link_column)
        row = column = 0
        for e in self.data:
            if filter_item.text() == list(e.values())[self._link_column]:
                self._table.insertRow(row)
                for v in e.values():
                    item = QtWidgets.QTableWidgetItem(str(v))
                    self._table.setItem(row, column, item)
                    column += 1
                row += 1
                column = 0


