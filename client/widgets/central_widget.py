from PySide2 import QtWidgets
from client.widgets.data_table import DataTable
from client.widgets.dialogs.add_country_dialog import AddCountryDialog
from client.widgets.dialogs.edit_country_dialog import EditCountyDialog


class CentralWidget(QtWidgets.QWidget):
    def __init__(self, parent):
        super().__init__(parent)
        self._db_handler = parent._db_handler
        self._parent = parent
        self._layout = QtWidgets.QVBoxLayout()
        self._layout.setContentsMargins(0, 0, 0, 0)

        # Proslediti odgovarajuci edit dialog
        self._primary_table_widget = DataTable(self, table_name="Države", add_dialog=AddCountryDialog(self._parent), 
            edit_dialog=EditCountyDialog(self._parent), delete_dialog=QtWidgets.QMessageBox(QtWidgets.QMessageBox.Critical,
            "Birsanje reda", "Da li ste sigurni da želite da obrišete red?", QtWidgets.QMessageBox.StandardButton.Yes | 
            QtWidgets.QMessageBox.StandardButton.No, parent = self))
        self._linked_table_widget = DataTable(self, self._primary_table_widget._table, 0, 0, "Naseljena mesta")

        self._primary_table_widget.populate_table(self._db_handler.get_all("drzava"), 
            {"name": "drzava", "columns": self._db_handler.get_column_names("drzava"), 
            "pk": self._db_handler.get_primary_keys("drzava")})
        self._linked_table_widget.populate_table(self._db_handler.get_all("naseljeno_mesto"),
            {"name": "naseljeno_mesto", "columns": self._db_handler.get_column_names("naseljeno_mesto"),
            "pk": self._db_handler.get_primary_keys("naseljeno_mesto")})

        self._layout.addWidget(self._primary_table_widget)
        self._layout.addSpacerItem(QtWidgets.QSpacerItem(10, 10))
        # self._layout.addSpacing(20)
        self._layout.addWidget(self._linked_table_widget)

        self.setLayout(self._layout)

    # FIXME: U MVC arhitekturi ove metode bi isle u kontroler
    def insert_data(self, table_name, row):
        """
        Zapisuje u bazu podataka red u tabelu
        :param table_name: naziv tabele u koju se red dodaje
        :param row: podaci u obliku recnika
        """
        self._db_handler.insert_one(table_name, list(row.keys()), list(row.values()))

    def edit_data(self, table_name, row, key_columns, key_values):
        """
        Azurira red u tabeli spram zadatog kriterijuma.
        :param table_name: naziv tabele u kojoj treba azurirati red
        :param row: podaci u obliku recnika dobijeni iz dijaloga
        :param key_columns: lista naziva kolona spram koje ce se izvrsiti seleckcija
        :param key_values: lista vrednosti koja se mora poklopiti za dobavljanje reda za izmenu
        """
        # FIXME: usaglasiti da u update_one se prosledjuju liste za kljuceve i vrednosti po kojima se
        # filtrira kolona za izmenu
        self._db_handler.update_one(table_name, list(row.keys()), list(row.values()), key_columns[0], key_values[0])

    def delete_data(self, table_name, key_columns, key_values):
        # FIXME: ispraviti da se po vise kolona moze vrsiti pretraga
        self._db_handler.delete_one(table_name, key_columns[0], key_values[0])
