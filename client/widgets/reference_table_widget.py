from PySide2 import QtWidgets, QtGui


class ReferenceTableWidget(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self._actions = {}
        

        self._layout = QtWidgets.QVBoxLayout()
        self.primary_table = QtWidgets.QTableWidget(10, 10)
        self.reference_table = QtWidgets.QTableWidget(10, 10)
        self.primary_toolbar = QtWidgets.QToolBar()
        self.reference_toolbar = QtWidgets.QToolBar()

        self.primary_table.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.primary_table.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)

        self.reference_table.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.reference_table.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)

        self._layout.setMenuBar(self.primary_toolbar)
        self._layout.addWidget(QtWidgets.QLabel("Primary table"))
        self._layout.addWidget(self.primary_table)
        self._layout.addSpacing(20)
        self._layout.addWidget(QtWidgets.QLabel("Reference table"))
        self._layout.addWidget(self.reference_table)

        self._populate_actions()
        self._bind_actions()

        self.setLayout(self._layout)

    def _populate_actions(self):
        self._actions["primary_add"] = QtWidgets.QAction(QtGui.QIcon("client/icons/table-insert-row.png"), "Add row")
        self._actions["primary_remove"] = QtWidgets.QAction(QtGui.QIcon("client/icons/table-delete-row.png"), "Remove row")

        self.primary_toolbar.addActions(list(self._actions.values()))

    def _bind_actions(self):
        self._actions["primary_add"].triggered.connect(self.add_row)
        self._actions["primary_remove"].triggered.connect(self.remove_row)

    def populate_primary(self, data): ...

    def populate_reference(self, data): ...



    # action functions
    def add_row(self, event):
        print(event)

    def remove_row(self, event):
        print(event)