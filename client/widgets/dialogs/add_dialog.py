from PySide2 import QtWidgets


class AddDialog(QtWidgets.QDialog):
    def __init__(self, parent=None, title="Add country"):
        super().__init__(parent)
        self.setWindowTitle(title)
        
        self._layout = QtWidgets.QGridLayout()

        self.button_box = QtWidgets.QDialogButtonBox(QtWidgets.QDialogButtonBox.Ok | QtWidgets.QDialogButtonBox.Close)
        self.button_box.accepted.connect(self.add_row)
        self.button_box.rejected.connect(self.reject)

        # self._layout.addWidget(self.button_box)
        # self.setLayout(self._layout)

    def add_row(self):
        # TODO: Iscitati podatke iz forme 
        # TODO: Proveriti ispravnost podataka (da li su svi obavezni uneti)