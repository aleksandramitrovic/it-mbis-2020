from PySide2 import QtWidgets


class AddCountryDialog(QtWidgets.QDialog):
    def __init__(self, parent=None, title="Add element"):
        super().__init__(parent)
        self.setWindowTitle(title)
        self.data = {}
        self.polity = {
            "Republika": "R",
            "Monarhija": "M",
            "Federacija": "F"
        }
        
        self._layout = QtWidgets.QGridLayout()

        self.sign_label = QtWidgets.QLabel("Oznaka:")
        self.sign_edit = QtWidgets.QLineEdit()
        self.name_label = QtWidgets.QLabel("Naziv:")
        self.name_edit = QtWidgets.QLineEdit()
        self.coa_label = QtWidgets.QLabel("Grb:") # coa - coat of arms
        self.coa_edit = QtWidgets.QLineEdit()
        self.flag_label = QtWidgets.QLabel("Zastava:")
        self.flag_edit = QtWidgets.QLineEdit()
        self.anthem_label = QtWidgets.QLabel("Himna:")
        self.anthem_edit = QtWidgets.QLineEdit()
        self.polity_label = QtWidgets.QLabel("Državno uređenje:")
        self.polity_combo = QtWidgets.QComboBox()
        self.polity_combo.addItems(list(self.polity.keys()))
        self.capital_city_country_label = QtWidgets.QLabel("Glavni grad drzava:")
        self.capital_city_country_edit = QtWidgets.QLineEdit()
        self.capital_city_label = QtWidgets.QLabel("Glavni grad:")
        self.capital_city_edit = QtWidgets.QSpinBox()
        self.capital_city_edit.setMinimum(-1)

        self.button_box = QtWidgets.QDialogButtonBox(QtWidgets.QDialogButtonBox.Ok | QtWidgets.QDialogButtonBox.Close)
        self.button_box.accepted.connect(self.accept)
        self.button_box.rejected.connect(self.reject)

        self._layout.addWidget(self.sign_label, 0, 0)
        self._layout.addWidget(self.sign_edit, 0, 1)
        self._layout.addWidget(self.name_label, 1, 0)
        self._layout.addWidget(self.name_edit, 1, 1)
        self._layout.addWidget(self.coa_label, 2, 0)
        self._layout.addWidget(self.coa_edit, 2, 1)
        self._layout.addWidget(self.flag_label, 3, 0)
        self._layout.addWidget(self.flag_edit, 3, 1)
        self._layout.addWidget(self.anthem_label, 4, 0)
        self._layout.addWidget(self.anthem_edit, 4, 1)
        self._layout.addWidget(self.polity_label, 5, 0)
        self._layout.addWidget(self.polity_combo, 5, 1)
        self._layout.addWidget(self.capital_city_country_label, 6, 0)
        self._layout.addWidget(self.capital_city_country_edit, 6, 1)
        self._layout.addWidget(self.capital_city_label, 7, 0)
        self._layout.addWidget(self.capital_city_edit, 7, 1)

        self._layout.addWidget(self.button_box, 8, 0, 1, 2)
        self.setLayout(self._layout)

    # redefinisanje metode accept
    def accept(self):
        if len(self.sign_edit.text()) != 3:
            msg = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Warning, "Neispravni podaci!",
            "Oznaka države mora da sadrži tačno 3 karakera!", parent = self)
            msg.addButton(QtWidgets.QMessageBox.Ok)
            msg.exec_()
            return
        if len(self.name_edit.text()) < 1 or len(self.name_edit.text()) > 120:
            msg = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Warning, "Neispravni podaci!",
            "Naziv države mora da sadrži najmanje 1 karakter, a najviše 120 karaktera!", parent = self)
            msg.addButton(QtWidgets.QMessageBox.Ok)
            msg.exec_()
            return
        # prikupljanje podataka iz forme
        self.data = {
            "DR_OZNAKA": self.sign_edit.text(),
            "DR_NAZIV": self.name_edit.text(),
            "DR_GRB": self.coa_edit.text() if self.coa_edit.text() else None,
            "DR_HIMNA": self.anthem_edit.text() if self.anthem_edit.text() else None,
            "DR_ZASTAVA": self.flag_edit.text() if self.flag_edit.text() else None,
            "NM_OZNAKA": self.capital_city_edit.value() if self.capital_city_edit.value() != -1 else None,
            "DUR_OZNAKA": self.polity[self.polity_combo.currentText()],
            "DRZ_DR_OZNAKA": self.capital_city_country_edit.text() if self.capital_city_country_edit.text() else None
        }
        self.clear_form()
        super().accept()

    def clear_form(self):
        self.sign_edit.clear()
        self.name_edit.clear()
        self.coa_edit.clear()
        self.flag_edit.clear()
        self.anthem_edit.clear()
        # za drzavno uredjenje mozemo ostaviti prethodno selektovano
        self.capital_city_country_edit.clear()
        self.capital_city_edit.setValue(-1)
