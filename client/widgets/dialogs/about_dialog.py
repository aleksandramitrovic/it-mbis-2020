from PySide2 import QtWidgets


class AboutDialog(QtWidgets.QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setWindowTitle("About mBIS")
        
        self._layout = QtWidgets.QVBoxLayout()
        self.information = QtWidgets.QLabel("""
        mBIS - Mali poslovni informacioni sistem
        Izradili studenti Infromacionih tehnologija
        kroz predmetni projekat Projektovanje informacionih sistema
        Akademske 2020/2021.
        Mentor: Aleksandra Mitrović
        Profesor: Branko Perišić
        """)
        self.button_box = QtWidgets.QDialogButtonBox(QtWidgets.QDialogButtonBox.Ok)
        self.button_box.accepted.connect(self.accept)


        self._layout.addWidget(self.information)
        self._layout.addWidget(self.button_box)
        self.setLayout(self._layout)