from PySide2 import QtWidgets, QtGui


class Modules(QtWidgets.QTreeWidget):
    
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setHeaderHidden(True)

        self.group_icon = QtGui.QIcon("client/icons/folder.png")
        self.table_icon = QtGui.QIcon("client/icons/table.png")

    def add_items(self, items:list):
        """
        Dodaje elemente u stablo iz liste elemenata.
        :param items: lista recnika na osnovu kog se formira stablo.
        """
        # Primer izgleda jednog elementa liste items
        # item = {
        #     "name": "Group",
        #     "tables" : [
        #         "Table 1", "Table 2"
        #     ]
        # }
        for i in items:
            item = QtWidgets.QTreeWidgetItem(self)
            item.setIcon(0, self.group_icon)
            item.setText(0, i["name"])
            for t in i["tables"]: # table in item
                child = QtWidgets.QTreeWidgetItem(item)
                child.setIcon(0, self.table_icon)
                child.setText(0, t)
                item.addChild(child)