from PySide2 import QtWidgets, QtGui, QtCore
from client.widgets.central_widget import CentralWidget
from client.widgets.dialogs.about_dialog import AboutDialog
from client.widgets.modules import Modules
from server.db_handler import DBHandler


class MainWindow(QtWidgets.QMainWindow):
    actions = {}

    def __init__(self, title, size, icon):
        super().__init__()
        self.resize(size[0], size[1])
        self.setWindowTitle(title)
        self.setWindowIcon(icon)

        self._db_handler = DBHandler()

        self._init_actions()
        self._bind_actions()

        self._create_menubar()
        self._create_toolbar()
        self._create_central_widget()
        self._create_dock_widget()
        self._create_status_bar()
    
    def _create_toolbar(self):
        self.toolbar = QtWidgets.QToolBar()
        self.toolbar.addActions(list(MainWindow.actions.values()))

        self.addToolBar(self.toolbar)
    
    def _create_menubar(self):
        self.menubar = QtWidgets.QMenuBar()
        self.file_menu = QtWidgets.QMenu("&File")
        self.help_menu = QtWidgets.QMenu("&Help")
        self.edit_menu = QtWidgets.QMenu("&Edit")

        self.menubar.addMenu(self.file_menu)
        self.menubar.addMenu(self.edit_menu)
        self.menubar.addMenu(self.help_menu)

        self._populate_file_menu()
        self._populate_help_menu()

        self.setMenuBar(self.menubar)

    def _create_central_widget(self):
        self.central_widget = CentralWidget(self)

        self.setCentralWidget(self.central_widget)

    def _create_dock_widget(self):
        self.modules_dock = QtWidgets.QDockWidget("Modules", self)
        self.modules = Modules(self.modules_dock)
        self.modules_dock.setWidget(self.modules)

        # FIXME: Ispraviti kako se kreiraju tabele
        self.modules.add_items([
            {
                "name": "Teritorijalna organizacija",
                "tables": self._db_handler.get_table_names()
            },
            {
                "name": "Organizacija poslovnih sistema",
                "tables": []
            },
            {
                "name": "Ljudski resursi",
                "tables": []
            },
            {
                "name": "Administracija korisnika",
                "tables": []
            },
            {
                "name": "Poslovanje",
                "tables": []
            },
            {
                "name": "Knjigovodstvo",
                "tables": []
            },
            {
                "name": "Proizvodnja",
                "tables": []
            },
            {
                "name": "Nabavka",
                "tables": []
            }
        ])

        self.addDockWidget(QtCore.Qt.LeftDockWidgetArea, self.modules_dock)


    def _create_status_bar(self):
        self.statusbar = QtWidgets.QStatusBar()
        self.statusbar.addWidget(QtWidgets.QLabel("mBIS - business system for everyone!"))
        
        self.setStatusBar(self.statusbar)

    def _populate_file_menu(self):
        self.file_menu.addAction(MainWindow.actions["open_table"])
        self.file_menu.addSeparator()
        self.file_menu.addAction(MainWindow.actions["quit"])

    def _populate_help_menu(self):
        self.help_menu.addAction(MainWindow.actions["help_about"])

    def _init_actions(self):
        MainWindow.actions["open_table"] = QtWidgets.QAction(QtGui.QIcon("client/icons/table--plus.png"), "&Open table")
        MainWindow.actions["help_about"] = QtWidgets.QAction(QtGui.QIcon("client/icons/information-white.png"), "&About MBIS")
        MainWindow.actions["quit"] = QtWidgets.QAction(QtGui.QIcon("client/icons/cross.png"), "&Quit")


    def _bind_actions(self):
        MainWindow.actions["open_table"].triggered.connect(self._open_table)
        MainWindow.actions["help_about"].triggered.connect(self._open_about_dialog)
        MainWindow.actions["quit"].triggered.connect(self.close)

    def _open_table(self):
        msg = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Information, "Funkcionalnost nedostupna!",
            "Dinamičko otvaranje tabele još uvek nije omogućeno!", parent = self)
        msg.addButton(QtWidgets.QMessageBox.Ok)
        msg.exec_()

    def _open_about_dialog(self):
        dialog = AboutDialog(self)
        dialog.exec_()


